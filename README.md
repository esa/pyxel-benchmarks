[![asv](http://img.shields.io/badge/benchmarked%20by-asv-blue.svg?style=flat)](https://esa.gitlab.io/pyxel/benchmarks/)

# Pyxel benchmarks

## About

Pyxel performance benchmarks.

## Running the benchmarks

Install 'asv'.

More info: https://asv.readthedocs.io/en/stable/benchmarks.html

```bash
$ cd pyxel-benchmarks
$ pip install -r requirements.txt
```

If you are running the tests for the first time on this computer
then you should first run this command:
```bash
$ asv machine
```

Then run the benchmarks
```bash
$ python start_benchmark.py
```

or with `uv`
```bash
$ uv run start_benchmark.py
or
$ uv run --python 3.11 start_benchmark.py
```


## Running the benchmarks locally

```bash
$ pip install asv
```

## More info

Remove benchmarks
```
$ uvx asv rm machine=XXX
```
