#  Copyright (c) European Space Agency, 2020.
#
#  This file is subject to the terms and conditions defined in file 'LICENCE.txt', which
#  is part of this Pyxel package. No part of the package, including
#  this file, may be copied, modified, propagated, or distributed except according to
#  the terms contained in the file ‘LICENCE.txt’.


def timeraw_import_pyxel():
    return "import pyxel"


def timeraw_import_pyxel_models():
    return "import pyxel.models"


def timeraw_import_pyxel_models_scene_generation():
    return "import pyxel.models.scene_generation"


def timeraw_import_pyxel_models_photon_collection():
    return "import pyxel.models.photon_collection"


def timeraw_import_pyxel_models_charge_generation():
    return "import pyxel.models.charge_generation"


def timeraw_import_pyxel_models_charge_collection():
    return "import pyxel.models.charge_collection"


def timeraw_import_pyxel_models_charge_transfer():
    return "import pyxel.models.charge_transfer"


def timeraw_import_pyxel_models_phasing():
    return "import pyxel.models.phasing"


def timeraw_import_pyxel_models_charge_measurement():
    return "import pyxel.models.charge_measurement"


def timeraw_import_pyxel_models_readout_electronics():
    return "import pyxel.models.readout_electronics"


def timeraw_import_pyxel_models_signal_transfer():
    return "import pyxel.models.signal_transfer"


def timeraw_import_pyxel_models_data_processing():
    return "import pyxel.models.data_processing"
