#  Copyright (c) European Space Agency, 2017, 2018, 2019, 2020, 2021, 2022.
#
#  This file is subject to the terms and conditions defined in file 'LICENCE.txt', which
#  is part of this Pyxel package. No part of the package, including
#  this file, may be copied, modified, propagated, or distributed except according to
#  the terms contained in the file ‘LICENCE.txt’.


from pathlib import Path


def timeraw_load_config():
    folder: Path = Path(__file__).parent
    filename: Path = folder / "exposure.yaml"

    setup = """import pyxel"""
    code = f"""_ = pyxel.load({filename.as_posix()!r})"""

    return code, setup


timeraw_load_config.pretty_name = "pyxel.load"
