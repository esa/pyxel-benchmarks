# /// script
# requires-python = ">=3.11"
# dependencies = [
#     "asv",
#     "virtualenv",
# ]
# ///
#  Copyright (c) European Space Agency, 2017, 2018, 2019, 2020, 2021, 2022.
#
#  This file is subject to the terms and conditions defined in file 'LICENCE.txt', which
#  is part of this Pyxel package. No part of the package, including
#  this file, may be copied, modified, propagated, or distributed except according to
#  the terms contained in the file ‘LICENCE.txt’.

import subprocess

from asv import __version__
from asv.machine import Machine


def get_machine() -> str:
    obj = Machine.load("~/.asv-machine.json")
    return obj.machine


def main():
    machine: str = get_machine()
    print(f"ASV version: {__version__}")
    print(f"Machine: {machine}")

    print("Run benchmarks")
    try:
        _ = subprocess.run(
            ["asv", "run", "--skip-existing-commits", "1.7..master"],
            timeout=3600*4,
        )
    except subprocess.TimeoutExpired as exc:
        print("Timeout !")

    print("Add new results to git")
    _ = subprocess.run(["git", "add", f"results/{machine}"])
    _ = subprocess.run(["git", "commit", "-m", f"New results from {machine}"])

    print("Push new results")
    _ = subprocess.run(["git", "push", "origin", "main"])
    _ = subprocess.run(["asv", "gh-pages", "--no-push"])
    _ = subprocess.run(["git", "push", "-f", "origin", "gh-pages"])


if __name__ == "__main__":
    main()
